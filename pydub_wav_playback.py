#!/usr/bin/python
# pydub_wav_playback.py
#
# Last Updated 2017.01.22
# Jun Go gojun077@gmail.com
#
# This script loads some .wav files and plays them using python's
# pydub module. Pydub requires ffmpeg to be installed as it uses
# ffmpeg to playback all media files.
#
# This file should be executed from the directory containing
# brainworkshop and its media resource sub-directories. This
# script can be executed in both Python 2 and 3.

import pydub
from pydub.playback import play

fpath = "./res/music/advance/Victory.wav"
song = pydub.AudioSegment.from_wav(fpath)
print("Now playing %s " % fpath + " with python pydub (ffmpeg)")
play(song)
