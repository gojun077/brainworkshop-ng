#!/usr/bin/python
# pyglet_wav_playback.py
#
# Last Updated 2017.01.14
# Jun Go gojun077@gmail.com
#
# This script loads some .wav files and plays them using python2's
# pyglet. Note that pyglet can only play uncompressed audio like
# WAV, however, the format block must be exactly 16 bytes which
# means the WAV file must not contain any miscellaneous tags.
# Pyglet is therefore not 100% compliant with the RIFF iface spec
#
# This file should be executed from the directory containing
# brainworkshop and its media resource files
#
# Note that Pyglet requires a window to be defined to be able to
# react to keypress events
#
# This script can be executed in both Python 2 and 3

import pyglet


fpath = "./res/music/advance/Concert.wav"
player = pyglet.media.Player()
source = pyglet.media.load(fpath)
player.queue(source)
print("Now playing %s " % fpath + " with python pyglet")
player.play()
