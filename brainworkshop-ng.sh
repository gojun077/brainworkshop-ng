#!/bin/bash

# The original path for this file is '/usr/bin/brainworkshop'
# The original path for the python script is '/usr/share/brainworkshop/'

BRW_CONFIGFILE=$HOME/Dropbox/.brainworkshop/data/.brainworkshop.ini
BRW_STATFILE=$HOME/Dropbox/.brainworkshop/data/.brainworkshop.stats
BRW_DATADIR=$HOME/Dropbox/.brainworkshop/data/

# Brainworkshop requires Python 2; on some distro's 'python' refers
# to Python 2, while on others 'python2' must be invoked directly.
python2 ~/Documents/brainworkshop-ng/brainworkshop.pyw \
    --configfile ~/Dropbox/.brainworkshop/data/.brainworkshop.ini \
    --statsfile ~/Dropbox/.brainworkshop/data/.brainworkshop.stats \
    --datadir ~/Dropbox/.brainworkshop/data/

# TODO create script to create symlink from $HOME/bin to /usr/bin/ for
# BW launch script
