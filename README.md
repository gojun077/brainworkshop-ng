brainworkshop-ng
================
This is a variant of Brainworkshop 4.8.7 available on
Sourceforge. I have made the following changes to the original
Python 2 source:

- Update syntax to work with  pyglet 1.2.4 (latest version as of
  2017-01-15)

- Remove all dependencies on avbin 7

- Reencode all `.ogg media` files as untagged `.wav` files

- misc. changes (disable pyglet OpenGL debug mode, etc.)

# Downloading Brainworkshop media files
The `res` subfolder in the `brainworkshop-ng` working directory
normally contains other subfolders where `.ogg` audio files are
stored. I chose not to commit these binary files into git but these
files are available through Google Drive. You can browse the `res`
directory at the following link:

https://drive.google.com/drive/folders/0BwWscpJ-DxlwUG9jWXJXcUMtTWM

You can download the entire `res` folder compressed as `...tar.xz`
from the following Google Drive link:

https://drive.google.com/file/d/0BwWscpJ-Dxlwd1JKNjNtMF9Ed2c

After downloading, move it to your `brainworkshop-ng` directory
and simply decompress the folder with `tar xvf BW_4.8.7_res.tar.xz`
This will decompress all media files into the `res` subfolder.
