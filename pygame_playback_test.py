#!/bin/bash/python
# pygame_playback_test.py
#
# Last Updated 2017-01-22
# Test playback of audio files using pygame for Python 3

import pygame
import time

fpath = "/home/archjun/SpiderOak Hive/Music/jazz/" \
        "Sarah_Vaughan_-_Smoke_Gets_In_Your_Eyes.ogg"
fpath1 = "./res//music/advance/Victory.wav"

pygame.init()
pygame.mixer.init()
pygame.mixer.music.load(fpath)
print("Playing 15s of an 'ogg' compressed audio file with pygame")
pygame.mixer.music.play()
time.sleep(15)
pygame.mixer.music.stop()

pygame.mixer.music.load(fpath1)
print("Playing 15s of a 'wav' uncompressed audio file with pygame")
pygame.mixer.music.play()
time.sleep(15)
pygame.mixer.music.stop()
